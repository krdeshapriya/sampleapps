package com.neoline.heteoas.controller;

import com.neoline.heteoas.model.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.server.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/user/all")
public class UserController {

    @GetMapping("/normal")
    public List<User> getAllUsers() {
        User user1 = new User("Kasun", 26);
        User user2 = new User("Dineth", 26);
        return Arrays.asList(user1, user2);
    }

    @GetMapping(value = "withheteoas", produces = MediaTypes.HAL_JSON_VALUE)
    public List<User> getAllUsersWithHeteoas() {
        //We can use ControllerLinkBuilder to create a link but it is deprecated. So use WebMvcLinkBuilder instead
        User user1 = new User("Kasun", 26);
        User user2 = new User("Dineth", 26);

        Link user1Link = WebMvcLinkBuilder.linkTo(UserController.class).slash(user1.getName()).withSelfRel();
        Link user2Link = WebMvcLinkBuilder.linkTo(UserController.class).slash(user2.getName()).withSelfRel();
        Link user2Link2 = WebMvcLinkBuilder.linkTo(UserController.class).slash(user2.getName()).slash(user2.getAge()).withRel("age");

        user1.add(user1Link);
        user2.add(user2Link, user2Link2);


        return Arrays.asList(user1, user2);
    }
}
